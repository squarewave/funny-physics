using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

using Unity.Physics;
using Unity.Physics.Extensions;

public class PlayerMovementSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        Entities.ForEach((ref Translation translation, ref PhysicsVelocity velocityData, in PlayerMovementData movement, in PhysicsMass massData) => {

            float3 velocity = new float3(movement.desiredVelocity, 0.0f);
            translation.Value += velocity * deltaTime;

            float3 impulse = new float3(movement.desiredImpulse, 0.0f);
            PhysicsComponentExtensions.ApplyLinearImpulse(ref velocityData, massData, impulse);

        }).Run();
    }
}
