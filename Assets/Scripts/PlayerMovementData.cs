using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
[Serializable]
public struct PlayerMovementData : IComponentData
{
    public float2 desiredVelocity;
    public float2 desiredImpulse;
}
