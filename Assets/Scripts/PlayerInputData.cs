using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
[Serializable]
public struct PlayerInputData : IComponentData
{
    public float baseSpeed;

    public KeyCode leftKey;
    public KeyCode rightKey;
    public KeyCode jumpKey;
}
