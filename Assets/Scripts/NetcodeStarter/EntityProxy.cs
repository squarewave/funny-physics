﻿using Unity.Entities;

public struct EntityProxy : IComponentData {
  public Entity proxyFor;
}
