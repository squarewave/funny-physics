﻿using System;
using System.Collections.Generic;
using System.IO;
using TMPro;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs.LowLevel.Unsafe;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UI;
using Math = System.Math;
using Object = UnityEngine.Object;
using Random = Unity.Mathematics.Random;

public class GameManager : MonoBehaviour {
  public static GameManager instance;
  public Texture2D cursorTexture;

  private int _playerIndex;

  private SimulationSystemGroup _simGroup;
  private PrePhysicsGroup _preSimGroup;
  private PostPhysicsGroup _postSimGroup;
  private Camera _camera;
  private World _world;
  private GameSystem _game;
  private int _highestSeenServerTick;
  private StreamWriter _log;
  private bool _victoryStarted;
  private float _physicsPausedUntil;

  // Start is called before the first frame update
  void Start() {
    instance = this;
    _camera = Camera.main;
    _world = World.DefaultGameObjectInjectionWorld;
    _game = _world.GetExistingSystem<GameSystem>();
    _preSimGroup = _world.GetExistingSystem<PrePhysicsGroup>();
    _preSimGroup.Enabled = false;
    _simGroup = _world.GetExistingSystem<SimulationSystemGroup>();
    _simGroup.Enabled = false;
    _postSimGroup = _world.GetExistingSystem<PostPhysicsGroup>();
    _postSimGroup.Enabled = false;
    _camera.transparencySortMode = TransparencySortMode.CustomAxis;
    _camera.transparencySortAxis = _camera.projectionMatrix * Vector3.down;
    Debug.Log($"Logging to \"${Application.persistentDataPath}\"");
    _log = File.CreateText(Application.persistentDataPath + "/logfile-" + DateTime.Now.ToFileTimeUtc() +  ".txt");
    Application.logMessageReceived += ApplicationOnlogMessageReceived;
    
    Screen.SetResolution(1600, 900, false);
    JobsUtility.JobWorkerCount = 1;
    Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);
  }

  private void ApplicationOnlogMessageReceived(string condition, string stacktrace, LogType type) {
    _log.Write(type.ToString());
    _log.Write(condition);
    _log.Write(stacktrace);
    _log.Write("\n");
    _log.Flush();
  }

  private void FixedUpdate() {
    if (_game.IsClient()) {
      _game.client.Tick(NetcodeTickMode.ReceiveOnly);
    } else if (_game.IsServer()) {
      _game.server.Tick(NetcodeTickMode.ReceiveOnly);
    }

    if (Input.GetKey(KeyCode.P)) {
      return;
    }
    
    int advanceTimes = 1;
    if (_game.IsClient()) {
      if (ServerTickStatic.serverTick >= _highestSeenServerTick) {
        advanceTimes = 0;
      } else if (ServerTickStatic.serverTick < _highestSeenServerTick - 32) {
        ServerTickStatic.serverTick = _highestSeenServerTick - 1;
        Debug.Log("Client tick too far behind server - fast-forwarding...");
      } else if (ServerTickStatic.serverTick < _highestSeenServerTick - 4) {
        advanceTimes = 2;
      }
    }
    
    for (int i = 0; i < advanceTimes; i++) {
      if (_game.IsServer()) {
        ServerTickStatic.serverTick++;
        _game.server.QueueBroadcast(new ServerTickMessage {
          serverTick = ServerTickStatic.serverTick,
        }, true);
      } else {
        ServerTickStatic.serverTick++;
      }
      _preSimGroup.Enabled = true;
      _preSimGroup.Update();
      _preSimGroup.Enabled = false;
      if (Time.fixedTime > _physicsPausedUntil) {
        _simGroup.Enabled = true;
        _simGroup.Update();
        _simGroup.Enabled = false; 
      }
      _postSimGroup.Enabled = true;
      _postSimGroup.Update();
      _postSimGroup.Enabled = false;
    }
    
    if (_game.IsClient()) {
      _game.client.Tick(NetcodeTickMode.SendAndReceive);
    } else if (_game.IsServer()) {
      _game.server.Tick(NetcodeTickMode.SendAndReceive);
    }
  }

  public void ProcessServerTick(int serverTick) {
    _highestSeenServerTick = math.max(serverTick, _highestSeenServerTick);
  }
}
