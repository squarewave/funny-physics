using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;

public class JoinARoomMenu : MonoBehaviour {
    public GameObject enableOnJoin;
    public GameObject enableOnBack;
    private TMP_InputField _roomIdInput;
    private Regex _invalidRoomCodeCharRegex;
    private TMP_Text _errorMessage;
    private bool _initialized;

    private void EnsureInit() {
        if (!_initialized) {
            _initialized = true;
            NetcodeLobbySystem.instance.OnError += ShowError;
            IceProtocolManager.instance.OnError += OnError;
            IceProtocolManager.instance.OnPeerConnected += OnPeerConnected;
        }
    }

    private void Deinit() {
        _initialized = false;
        NetcodeLobbySystem.instance.OnError -= ShowError;
        IceProtocolManager.instance.OnError -= OnError;
        IceProtocolManager.instance.OnPeerConnected -= OnPeerConnected;
    }

    private void OnError(int peerId) {
        ShowError();
    }

    private void OnPeerConnected(int peerId) {
        IceProtocolManager.instance.StartClient(peerId);
        enableOnJoin.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OnRoomCodeChanged(string value) {
        var newVal = _invalidRoomCodeCharRegex.Replace(value, "").ToUpper();
        if (newVal.Length > 4) {
            newVal = newVal.Substring(0, 4);
        }
        _roomIdInput.text = newVal;
    }

    public void JoinRoom() {
        EnsureInit();
        _errorMessage.gameObject.SetActive(false);
        var roomId = _roomIdInput.text;
        NetcodeLobbySystem.instance.JoinRoom(roomId);
    }

    public void GoBack() {
        Deinit();
        _errorMessage.gameObject.SetActive(false);
        enableOnBack.SetActive(true);
        gameObject.SetActive(false);
    }

    private void ShowError() {
        if (gameObject.activeInHierarchy) {
            _errorMessage.gameObject.SetActive(true);
        }
    }

    private void Start() {
        _invalidRoomCodeCharRegex = new Regex(@"[^0-9a-zA-Z]");
        _roomIdInput = transform.Find("RoomIdInput").GetComponent<TMP_InputField>();
        _errorMessage = transform.Find("ErrorMessage").GetComponent<TMP_Text>();
        _roomIdInput.Select();
    }
}
