﻿using System.Collections.Generic;
using System.Threading;
using Unity.Entities;
using UnityEngine;

[UpdateInGroup(typeof(PrePhysicsGroup))]
[AlwaysUpdateSystem]
public class GameInputSystem : ComponentSystem {
  public int playerId;

  private readonly Dictionary<int, int> _unitsToPlayers =
    new Dictionary<int, int>();

  private int _turnIndex;
  private int _latestPlayerId;
  private GameSystem _game;

  private readonly Dictionary<int, UnitMicroInputMessage> _unitMicroInputs =
    new Dictionary<int, UnitMicroInputMessage>();

  protected override void OnCreate() {
    InitEntityQueryCache(30);
    _game = World.GetOrCreateSystem<GameSystem>();
  }

  public int GetPlayerForUnit(int unitId) {
    if (_unitsToPlayers.TryGetValue(unitId, out int playerId)) {
      return playerId;
    }

    Debug.LogError($"Player does not exist for unit {unitId}");
    return int.MinValue;
  }

  public int GetNewPlayerId() {
    if (!_game.IsServer()) {
      Debug.LogError("Client requesting a new player id.");
      return -1;
    }

    return Interlocked.Increment(ref _latestPlayerId);
  }

  public void RegisterUnit(int unitId, int playerId) {
    _unitsToPlayers[unitId] = playerId;
  }

  public void SetMicroInputState(UnitMicroInputMessage message) {
    _unitMicroInputs[message.unitId] = message;
  }

  public Vector3 GetUnitMousePosition(NetcodeObject unit) {
    if (unit.playerId == playerId) {
      return InputSingleton.instance.GetUnitMousePosition();
    } else if (_unitMicroInputs.TryGetValue(unit.id, out var microInput)) {
      return microInput.mouseWorldPosition;
    }

    return default;
  }

  public bool GetUnitMouseDown(NetcodeObject unit) {
    if (unit.playerId == playerId) {
      return InputSingleton.instance.GetUnitLeftMouseButton();
    } else if (_unitMicroInputs.TryGetValue(unit.id, out var microInput)) {
      return microInput.leftMouseButton;
    }

    return default;
  }

  protected override void OnUpdate() {
  }
}
