﻿#if UNITY_EDITOR
using System;
using Unity.Entities;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(NetcodeSettings))]
public class NetcodeSettingsEditor : Editor {
  private SerializedProperty defaultHostProperty;

  private void OnEnable() {
    defaultHostProperty = serializedObject.FindProperty("defaultHost");
  }

  public override void OnInspectorGUI() {
    serializedObject.Update();
    EditorGUILayout.PropertyField(defaultHostProperty);
    
    var settings = target as NetcodeSettings;
    
    GameSystem game = null;
    if (World.DefaultGameObjectInjectionWorld != null) {
      game = World.DefaultGameObjectInjectionWorld.GetExistingSystem<GameSystem>();
    }
    
    if (game == null || game.started) {
      GUI.enabled = false;
    }

    serializedObject.ApplyModifiedProperties();
  }
}
#endif
