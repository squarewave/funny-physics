﻿using System;
using Unity.Collections;
using Unity.Entities;

public interface INetcodeMessage<T> : IDisposable where T : INetcodeMessage<T> {
  bool IsReliable();

  bool Process(World world,
               int sendingPlayerId,
               bool isServer,
               ref bool skipDispose);

  bool Serialize(NativeArray<byte> buffer,
                 ref int offset,
                 int bufferMask);
}
