﻿using Unity.Entities;
using Unity.Physics;

public enum TemporaryPhysicsMassLifecycle {
  TurnTransition,
  Manual,
}

public struct TemporaryPhysicsMass : IComponentData {
  public PhysicsMass oldMass;
  public TemporaryPhysicsMassLifecycle lifecycle;
}
