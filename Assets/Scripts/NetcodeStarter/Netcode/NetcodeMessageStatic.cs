﻿using Unity.Collections;
using UnityEngine;

public static class NetcodeMessageStatic {
  public static bool SerializeMessage<T>(NativeArray<byte> buffer,
                                         int seqNo,
                                         T message,
                                         out int serializedSize,
                                         ref int offset,
                                         int bufferMask)
    where T : INetcodeMessage<T> {
    serializedSize = 0;
    int startOffset = offset;
    NativeArrayUtils.Write(buffer, ref offset, seqNo, bufferMask);
    int sizeOffset = offset;
    NativeArrayUtils.Write(buffer, ref offset, 0, bufferMask); // placeholder: adjust below
    NativeArrayUtils.Write(buffer, ref offset, (int) NetcodeMessageHandling.MessageIdForType<T>(), bufferMask);
    int bitOffset = offset * 8;
    if (!message.Serialize(buffer, ref bitOffset, bufferMask)) {
      Debug.LogError($"Message too large to store: {NetcodeMessageHandling.MessageIdForType<T>()}");
      return false;
    }
    offset = bitOffset / 8 + (bitOffset % 8 != 0 ? 1 : 0);
    serializedSize = offset - startOffset;
    NativeArrayUtils.Write(buffer, ref sizeOffset, serializedSize, bufferMask);
    return true;
  }

  public static bool DeserializeMessageHeader(NativeArray<byte> native,
                                              out NetcodeMessageHeader header,
                                              ref int offset,
                                              int bufferMask = NativeArrayUtils.DefaultBufferMask) {
    header = new NetcodeMessageHeader();
    if (!NativeArrayUtils.Read(native, ref offset, out header.seqNo, bufferMask)) {
      Debug.LogError($"Message too large to read");
      return false;
    }

    if (!NativeArrayUtils.Read(native, ref offset, out header.size, bufferMask)) {
      Debug.LogError($"Message too large to read");
      return false;
    }

    if (!NativeArrayUtils.Read(native, ref offset, out int intMessageId, bufferMask)) {
      Debug.LogError($"Message too large to read");
      return false;
    }

    if (intMessageId > (int) NetcodeMessageId.Count || intMessageId < 0) {
      Debug.LogError("Invalid message id");
      return false;
    }

    header.messageTypeId = (NetcodeMessageId) intMessageId;

    return true;
  }
}
