﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Unity.Entities;

public class NetcodeServer : NetcodeCommon {
  private readonly ConcurrentDictionary<IntPtr, NetcodeChannel> _netcodeChannels =
    new ConcurrentDictionary<IntPtr, NetcodeChannel>();

  public NetcodeServer(World world) {
    this.world = world;
    isServer = true;
  }

  public void Start() {
  }

  public void QueueBroadcast<T>(T message, bool excludeHost, int excludePlayerId = -1) where T : INetcodeMessage<T> {
    foreach (var channel in _netcodeChannels.Values) {
      if (channel.playerId != excludePlayerId) {
        QueueMessageInternal(message, channel);
      }
    }

    if (!excludeHost) {
      QueueMessageInternal(message, toServerChannel);
    }
  }

  public void QueueClientMessage<T>(int playerId, T message) where T : INetcodeMessage<T> {
    if (playerId == ServerPlayerId) {
      QueueMessageInternal(message, toServerChannel);
    }
    foreach (var channel in _netcodeChannels.Values) {
      if (channel.playerId != playerId) {
        continue;
      }

      QueueMessageInternal(message, channel);
    }
  }

  public void Tick(NetcodeTickMode mode) {
    foreach (var keyValuePair in _netcodeChannels) {
      var channel = keyValuePair.Value;
      ProcessIncomingMessages(channel);
    }
    ProcessIncomingMessages(toServerChannel);

    if (mode == NetcodeTickMode.ReceiveOnly) {
      return;
    }

    SortBuffers();
    foreach (var keyValuePair in _netcodeChannels) {
      var peer = keyValuePair.Key;
      var channel = keyValuePair.Value;
      
      SendAcksAndQueuedMessages(channel, peer);
    }
    SendAcksAndQueuedMessages(toServerChannel, IntPtr.Zero);
  }

  private void SendCallback(IAsyncResult ar) {
    NetcodeSendBuffer so = (NetcodeSendBuffer) ar.AsyncState;

#if NETCODE_DEBUG_LOGGING
    int numBytes =
#endif
    socket.EndSendTo(ar);
    
    Interlocked.Exchange(ref so.delivered, 1);
#if NETCODE_DEBUG_LOGGING
    Debug.Log($"SEND: {numBytes}, {BitConverter.ToString(so.buffer.Take(numBytes).ToArray())}");
#endif
  }

  public override void ReceiveCallback(IntPtr peer, IntPtr buf, int size) {
    if (!_netcodeChannels.TryGetValue(peer, out NetcodeChannel channel)) {
      channel = new NetcodeChannel {
        playerId = world.GetExistingSystem<GameInputSystem>().GetNewPlayerId()
      };
      while (!_netcodeChannels.TryAdd(peer, channel)) { }
    }
    
    unsafe { CopyPacketToRingBufferOmt(channel, (byte*)buf, size); }
  }
}
