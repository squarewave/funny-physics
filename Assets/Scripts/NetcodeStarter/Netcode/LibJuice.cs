﻿using System;
using System.Runtime.InteropServices;

public class LibJuice {
  public const int JUICE_MAX_SDP_STRING_LEN = 4096;
  
  public enum juice_state_t {
    JUICE_STATE_DISCONNECTED,
    JUICE_STATE_GATHERING,
    JUICE_STATE_CONNECTING,
    JUICE_STATE_CONNECTED,
    JUICE_STATE_COMPLETED,
    JUICE_STATE_FAILED
  }
  
  [StructLayout(LayoutKind.Sequential)]
  public struct juice_turn_server_t {
    [MarshalAs(UnmanagedType.LPStr)]
    public string host;
    [MarshalAs(UnmanagedType.LPStr)]
    public string username;
    [MarshalAs(UnmanagedType.LPStr)]
    public string password;
    [MarshalAs(UnmanagedType.U2)]
    public ushort port;
  }
  
  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  public delegate void juice_cb_state_changed_t(IntPtr agent, juice_state_t state, IntPtr user_ptr);
  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  public delegate void juice_cb_candidate_t(IntPtr agent, IntPtr sdp, IntPtr user_ptr);
  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  public delegate void juice_cb_gathering_done_t(IntPtr agent, IntPtr user_ptr);
  [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
  public delegate void juice_cb_recv_t(IntPtr agent, IntPtr data, ulong size, IntPtr user_ptr);
  
  [StructLayout(LayoutKind.Sequential)]
  public struct juice_config_t {
    [MarshalAs(UnmanagedType.LPStr)]
    public string stun_server_host;
    [MarshalAs(UnmanagedType.U2)]
    public ushort stun_server_port;

    public IntPtr turn_servers;
    [MarshalAs(UnmanagedType.SysInt)]
    public int turn_servers_count;
    
    [MarshalAs(UnmanagedType.LPStr)]
    public string bind_address;
    [MarshalAs(UnmanagedType.U2)]
    public ushort local_port_range_begin;
    [MarshalAs(UnmanagedType.U2)]
    public ushort local_port_range_end;
    
    [MarshalAs(UnmanagedType.FunctionPtr)]
    public IntPtr cb_state_changed;
    [MarshalAs(UnmanagedType.FunctionPtr)]
    public IntPtr cb_candidate;
    [MarshalAs(UnmanagedType.FunctionPtr)]
    public IntPtr cb_gathering_done;
    [MarshalAs(UnmanagedType.FunctionPtr)]
    public IntPtr cb_recv;

    public IntPtr user_ptr;
  }

  private const string DllName = "juice.dll";

  [DllImport(DllName, EntryPoint = "juice_create", CallingConvention = CallingConvention.Cdecl)]
  public static extern IntPtr juice_create(ref juice_config_t config);

  [DllImport(DllName, EntryPoint = "juice_destroy", CallingConvention = CallingConvention.Cdecl)]
  public static extern void juice_destroy(IntPtr agent);

  [DllImport(DllName, EntryPoint = "juice_get_local_description", CallingConvention = CallingConvention.Cdecl)]
  public static extern int juice_get_local_description(IntPtr agent, IntPtr buffer, ulong size);
  
  [DllImport(DllName, EntryPoint = "juice_set_remote_description", CallingConvention = CallingConvention.Cdecl)]
  public static extern int juice_set_remote_description(IntPtr agent, IntPtr buffer);
  
  [DllImport(DllName, EntryPoint = "juice_gather_candidates", CallingConvention = CallingConvention.Cdecl)]
  public static extern int juice_gather_candidates(IntPtr agent);
  
  [DllImport(DllName, EntryPoint = "juice_get_state", CallingConvention = CallingConvention.Cdecl)]
  public static extern juice_state_t juice_get_state(IntPtr agent);
  
  [DllImport(DllName, EntryPoint = "juice_add_remote_candidate", CallingConvention = CallingConvention.Cdecl)]
  public static extern int juice_add_remote_candidate(IntPtr agent, IntPtr sdp);
  
  [DllImport(DllName, EntryPoint = "juice_set_remote_gathering_done", CallingConvention = CallingConvention.Cdecl)]
  public static extern int juice_set_remote_gathering_done(IntPtr agent);
  
  [DllImport(DllName, EntryPoint = "juice_send", CallingConvention = CallingConvention.Cdecl)]
  public static extern int juice_send(IntPtr agent, IntPtr data, ulong size);
}
