﻿using System.Linq;
using Unity.Collections;
using Unity.Entities;

[GenerateMessageSerialization]
public struct ObjectInstantiatedMessage : INetcodeMessage<ObjectInstantiatedMessage> {
  public NetcodeObjectInitializer initializer;
  [FieldQuantization(QuantizationConstants.MinNetObjId, QuantizationConstants.MaxNetObjId)]
  public NativeArray<int> childIds;

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    if (isServer) {
      return false;
    }
    
    world.GetExistingSystem<GameSystem>().PurgeTempEntities();
    world.GetExistingSystem<GameSystem>().InstantiateObject(initializer, childIds);
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public bool IsReliable() {
    return true;
  }

  public void Dispose() {
    childIds.Dispose();
  }
}