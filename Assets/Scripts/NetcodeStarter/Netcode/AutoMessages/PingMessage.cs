﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageSerialization]
public struct PingMessage : INetcodeMessage<PingMessage> {
  [FieldQuantization(QuantizationConstants.MinServerTick, QuantizationConstants.MaxServerTick)]
  public int estimatedServerTick;
  [FieldQuantization(0, 1000000.0f, 32)]
  public float lastPingTime;

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    world.GetExistingSystem<GameSystem>().ProcessPingMessage(sendingPlayerId, this);
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public bool IsReliable() {
    return true;
  }

  public void Dispose() {
  }
}
