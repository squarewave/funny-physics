﻿using System;
using Unity.Collections;
using Unity.Mathematics;

[GenerateMessageFieldSerialization]
public struct NetcodeObjectInitializer {
  [FieldQuantization(QuantizationConstants.MinNetObjId, QuantizationConstants.MaxNetObjId)]
  public int id;
  [FieldQuantization(QuantizationConstants.MinPlayerId, QuantizationConstants.MaxPlayerId)]
  public int playerId;
  [FieldQuantization(QuantizationConstants.MinPrefabIndex, QuantizationConstants.MaxPrefabIndex)]
  public int prefabIndex;
  [FieldQuantization("TrackedBodyState.PositionMin", "TrackedBodyState.PositionMax", "TrackedBodyState.PositionBits")]
  public float3 position;
  public quaternion rotation;
  public bool isDeathPrefab;
  public bool isInactive;
}
