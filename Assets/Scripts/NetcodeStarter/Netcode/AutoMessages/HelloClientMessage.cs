﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageSerialization]
public struct HelloClientMessage : INetcodeMessage<HelloClientMessage> {
  [FieldQuantization(QuantizationConstants.MinPlayerId, QuantizationConstants.MaxPlayerId)]
  public int playerId;
  public NativeArray<NetcodeObjectInitializer> netcodeObjectInitializers;
  [FieldQuantization(QuantizationConstants.MinNetObjId, QuantizationConstants.MaxNetObjId)]
  public NativeArray<int> childIds;
  public NativeArray<PlayerData> players;

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    if (isServer) {
      return false;
    }
    return world.GetExistingSystem<GameSystem>().InitializeClient(this);
  }

  public bool IsReliable() {
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public void Dispose() {
    netcodeObjectInitializers.Dispose();
    players.Dispose();
    childIds.Dispose();
  }
}