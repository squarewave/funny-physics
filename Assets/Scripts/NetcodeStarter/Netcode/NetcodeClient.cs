﻿using System;
using System.Threading;
using Unity.Entities;

public enum NetcodeTickMode {
  ReceiveOnly,
  SendAndReceive
}

public class NetcodeClient : NetcodeCommon {
  private int _nextSeqNo;

  private int _reestablishListenerInTicks = int.MaxValue;
  private IntPtr _host;

  public NetcodeClient(World world) {
    this.world = world;
  }

  public void Start(IntPtr hostPeer) {
    _host = hostPeer;
  }

  public void Tick(NetcodeTickMode mode) {
    ProcessIncomingMessages(toServerChannel);

    if (Interlocked.Decrement(ref _reestablishListenerInTicks) == 0) {
      Interlocked.CompareExchange(ref _reestablishListenerInTicks, int.MaxValue, 0);
    }

    if (mode == NetcodeTickMode.ReceiveOnly) {
      return;
    }

    SortBuffers();
    SendAcksAndQueuedMessages(toServerChannel, _host);
  }

  public override void ReceiveCallback(IntPtr peer, IntPtr buf, int size) {
      unsafe { CopyPacketToRingBufferOmt(toServerChannel, (byte*)buf, size); }
  }
}
