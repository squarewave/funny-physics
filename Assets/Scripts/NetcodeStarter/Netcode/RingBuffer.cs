﻿using System;
using System.Collections.Generic;
using System.Text;

public class RingBuffer {
  private int _writeHead = 0;
  private int _readHead = 0;
  private int _peekStart = 0;
  
  public RingBuffer(int size) {
    Buf = new byte[size];
    _peekStart = -1;
  }
  
  public byte[] Buf { get; }

  public int WriteHead => _writeHead & (Buf.Length - 1);
  public int ReadHead => _readHead & (Buf.Length - 1);

  public int ContiguousRemainingWrite => Buf.Length - WriteHead;
  public int ContiguousRemainingRead => Math.Min(_writeHead - _readHead, Buf.Length - ReadHead);
  public int RemainingRead => _writeHead - _readHead;

  public void AdvanceWriteHead(int amount) {
    _writeHead += amount;
  }
  
  public void AdvanceReadHead() {
    AdvanceReadHead(ContiguousRemainingRead);
  }
  
  public void AdvanceReadHead(int amount) {
    if (_readHead + amount > _writeHead) {
      throw new InvalidOperationException("Tried to advance read head past write head.");
    }
    _readHead += amount;
  }

  public void Write(byte[] bytes) {
    if (bytes.Length > Buf.Length) {
      throw new InvalidOperationException("BulkFrame larger than buffer");
    }
    int count = Math.Min(bytes.Length, ContiguousRemainingWrite);
    Buffer.BlockCopy(bytes, 0, Buf, WriteHead, count);
    AdvanceWriteHead(count);
    if (count < bytes.Length) {
      Buffer.BlockCopy(bytes, count, Buf, WriteHead, bytes.Length - count);
    }
  }

  public void WriteAscii(string str) {
    Write(Encoding.ASCII.GetBytes(str));
  }

  public void Write(char c) {
    Write(Convert.ToByte(c));
  }
  public void Write(byte b) {
    Buf[WriteHead] = b;
    AdvanceWriteHead(1);
  }

  public void WriteCRLF() {
    Write(13);
    Write(10);
  }

  public byte ReadOne() {
    var result = Buf[ReadHead];
    AdvanceReadHead(1);
    return result;
  }

  public bool ReadLine(out string result) {
    var bytes = new List<byte>();
    while (RemainingRead > 0 && PeekOne() != 13 && PeekOne() != 10) {
      bytes.Add(ReadOne());      
    }

    if (ReadCRLF()) {
      result = Encoding.UTF8.GetString(bytes.ToArray());
      return true;
    }

    result = null;
    return false;
  }

  public bool ReadCRLF() {
    if (RemainingRead < 2) {
      _readHead = _writeHead;
      return false;
    }
    if (ReadOne() == 13 && ReadOne() == 10) {
      return true;
    }
    return false;
  }

  public byte PeekOne() {
    return Buf[ReadHead];
  }
  
  public void MaybeStartPeek() {
    if (_peekStart == -1) {
      _peekStart = _readHead;
    }
  }

  public void EndPeek() {
    _readHead = _peekStart;
    _peekStart = -1;
  }

  public void CancelPeek() {
    _peekStart = -1;
  }

  public int Read(byte[] output) {
    int count = Math.Min(output.Length, ContiguousRemainingRead);
    Buffer.BlockCopy(Buf, ReadHead, output, 0, count);
    AdvanceReadHead(count);
    if (count < output.Length && ReadHead != WriteHead) {
      Buffer.BlockCopy(Buf, ReadHead, output, count, ContiguousRemainingRead);
      count += ContiguousRemainingRead;
      AdvanceReadHead(ContiguousRemainingRead);
    }
    return count;
  }
}
