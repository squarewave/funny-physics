﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

public class LobbyWindow : MonoBehaviour {
  public RectTransform playernamePrefab;
  public RectTransform playernamesRegion;
  public GameObject enableOnSuccess;
  public TMP_InputField roomCode;

  public float nextPlayernameY;
  public float playernameMarginY;

  private GameSystem _game;
  private GameInputSystem _input;

  private EntityQuery _playersQuery;

  private readonly Dictionary<int, RectTransform> _playerIdToPlayernameCard =
    new Dictionary<int, RectTransform>();

  private World _world;
  private Transform _startButton;
  private bool _isServer;

  private void Start() {
    _world = World.DefaultGameObjectInjectionWorld;
    _game = _world.GetOrCreateSystem<GameSystem>();
    _input = _world.GetOrCreateSystem<GameInputSystem>();
    _playersQuery =
      _world.EntityManager.CreateEntityQuery(typeof(PlayerData));
    _game.common.AddPersistentListener<PlayerLobbyStateMessage>(OnPlayerLobbyState);
    roomCode.text = NetcodeLobbySystem.instance.activeRoomId;
    IceProtocolManager.instance.OnError += OnError;
    StartCoroutine(PollAndSendLobbyState());
    _startButton = transform.Find("StartButton");
    _game.Start();
    _startButton.gameObject.SetActive(_game.IsServer()); 
  }

  private void OnError(int obj) {
  }

  public void OnRoomCodeChange() {
    roomCode.text = NetcodeLobbySystem.instance.activeRoomId;
  }

  IEnumerator PollAndSendLobbyState() {
    var waitOneHalfSecond = new WaitForSeconds(.5f);
    while (true) {
      var players = _playersQuery.ToComponentDataArray<PlayerData>(Allocator.Temp);

      if (_game.IsServer()) {
        var playerLobbyState = new PlayerLobbyStateMessage {
          playerIds = new NativeArray<int>(players.Length, Allocator.Temp),
        };

        for (int i = 0; i < players.Length; i++) {
          playerLobbyState.playerIds[i] = players[i].id;
        }

        _game.server.QueueBroadcast(playerLobbyState, false);
        playerLobbyState.Dispose();
      }

      players.Dispose();

      yield return waitOneHalfSecond;
    }
  }

  private void OnPlayerLobbyState(PlayerLobbyStateMessage message) {
    for (int i = 0; i < message.playerIds.Length; i++) {
      var playerId = message.playerIds[i];

      AddOrUpdatePlayerCard(playerId);
    }
  }

  private void OnDestroy() {
    _game.common.RemovePersistentListener<PlayerLobbyStateMessage>(OnPlayerLobbyState);
  }

  private void AddOrUpdatePlayerCard(int playerId) {
    if (!_playerIdToPlayernameCard.TryGetValue(playerId, out var card)) {
      card = Instantiate(playernamePrefab, playernamesRegion);
      var pos = card.anchoredPosition;
      pos.y = nextPlayernameY;
      card.anchoredPosition = pos;
      nextPlayernameY -= playernamePrefab.rect.height + playernameMarginY;
      _playerIdToPlayernameCard[playerId] = card;
    }
  }

  public void Ready() {
    if (enableOnSuccess != null) {
      enableOnSuccess.SetActive(true);
    }
    Destroy(gameObject);
  }
}
