﻿using System;
using System.Collections.Generic;
using System.Threading;
using Unity.Entities;
using UnityEngine;

public class OpeningMenuDialogue : MonoBehaviour {
  public GameObject enableOnCreateARoom;
  public GameObject enableOnJoinARoom;
  private bool _initialized;

  private void EnsureInit() {
    if (!_initialized) {
      _initialized = true;
      NetcodeLobbySystem.instance.OnError += ShowError;
      NetcodeLobbySystem.instance.OnRoomJoined += OnRoomJoined;
    }
  }

  private void OnRoomJoined() {
    enableOnCreateARoom.SetActive(true);
    gameObject.SetActive(false);
  }

  public void CreateARoom() {
    EnsureInit();
    NetcodeLobbySystem.instance.CreateARoom();
  }

  private void ShowError() {
  }

  public void JoinARoom() {
    enableOnJoinARoom.SetActive(true);
    gameObject.SetActive(false);
  }
}
