using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

using UnityEngine;

public class PlayerInputSystem : SystemBase
{
    protected override void OnUpdate()
    {

        Entities
            .WithoutBurst() // For calling into Camera
            .ForEach((ref PlayerMovementData movement, in PlayerInputData input, in Translation translation) => {

            float left = 0.0f;
            float right = 0.0f;

            if (Input.GetKey(input.leftKey)) left = -input.baseSpeed;
            if (Input.GetKey(input.rightKey)) right = input.baseSpeed;

            movement.desiredVelocity.x = left + right;

            float jump = 0.0f;

            if (movement.desiredVelocity.y > 0.0f)
            {
                jump = movement.desiredVelocity.y - (input.baseSpeed / 30.0f);

                if (jump < 0.0f) jump = 0.0f;
            }
            else if (Input.GetKeyDown(input.jumpKey))
            {
                    jump = input.baseSpeed * 2.0f;
            }
            movement.desiredVelocity.y = jump;

            float2 recoil = float2.zero;

            if(Input.GetMouseButtonDown(0))
            {
                float3 mousePosition = Input.mousePosition;
                mousePosition.z = Camera.main.nearClipPlane;

                float3 launcherDirection = Camera.main.ScreenToWorldPoint(mousePosition);
                launcherDirection.z = 0.0f;

                float3 impulseDirection = translation.Value - launcherDirection;
                impulseDirection = math.normalize(impulseDirection);

                recoil = new float2(impulseDirection.x, impulseDirection.y);
                recoil *= input.baseSpeed;
            }
            movement.desiredImpulse = recoil;

        }).Run();
    }
}
