﻿using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.Windows;

namespace Editor {
  public static class MessagesCodeGen {
    [MenuItem("Code generation/Generate message code")]
    private static void GenerateMessageCode() {
      File.WriteAllBytes(Application.dataPath + "/Scripts/Generated/NetcodeMessageSerialization.cs",
                         Encoding.UTF8.GetBytes(MessageCodeGeneration.GenerateCode()));
      Debug.Log("Generated code!");
    }
  }
}
